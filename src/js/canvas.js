import utils from "./utils";

import platform from "../img/platform.png";
import hills from "../img/hills.png";
import background from "../img/background.png";
import platformSmallTall from "../img/platformSmallTall.png";

import spriteRunLeft from "../img/spriteRunLeft.png";
import spriteRunRight from "../img/spriteRunRight.png";
import spriteStandLeft from "../img/spriteStandLeft.png";
import spriteStandRight from "../img/spriteStandRight.png";

const canvas = document.querySelector("canvas");
const ctx = canvas.getContext("2d");

canvas.width = 1024;
canvas.height = 570;

const gravity = 1.5;

class Player {
  constructor() {
    this.speed = 10;
    this.position = {
      x: 100,
      y: 0,
    };
    this.width = 66;
    this.height = 150;
    this.velocity = {
      x: 0,
      y: 20,
    };
    this.image = createImage(spriteStandRight);
    this.frames = 0;
    this.sprites = {
      stand: {
        rigth: createImage(spriteStandRight),
        cropWidth: 177,
        width:this.width,
        left: createImage(spriteStandLeft),
      },
      run: {
        rigth: createImage(spriteRunRight),
        cropWidth: 340,
        width:127.875,
        left: createImage(spriteRunLeft),
      }
    }

    this.currentSprite = this.sprites.stand.rigth;
    this.currentCropWidth = this.sprites.stand.cropWidth;
  }

  draw() {
    ctx.drawImage(
      this.currentSprite,
      this.currentCropWidth * this.frames,
      0,
      this.currentCropWidth,
      400,
      this.position.x,
      this.position.y,
      this.width,
      this.height
    );
  }

  update() {
    this.frames ++
    if (this.frames > 28) {
      this.frames = 0
    }
    this.draw();
    this.position.y += this.velocity.y;
    this.position.x += this.velocity.x;
    if (this.position.y + this.height + this.velocity.y <= canvas.height) {
      this.velocity.y += gravity;
    } else {
    }
  }
}

class Platform {
  constructor({ x, y, image }) {
    this.position = {
      x: x,
      y: y,
    };

    this.image = image;
    this.width = image.width;
    this.height = image.height;
  }

  draw() {
    ctx.drawImage(this.image, this.position.x, this.position.y);
  }
}

class GenericObjet {
  constructor({ x, y, image }) {
    this.position = {
      x: x,
      y: y,
    };

    this.image = image;
    this.width = image.width;
    this.height = image.height;
  }

  draw() {
    ctx.drawImage(this.image, this.position.x, this.position.y);
  }
}

const createImage = (imageSrc) => {
  const image = new Image();
  image.src = imageSrc;
  return image;
};

const platformImage = createImage(platform);
const platformSmallTallImage = createImage(platformSmallTall);
let player;
let plataforms = [];

let genericObjets = [];

let scrollOffset = 0;

function init() {
  player = new Player();
  plataforms = [
    new Platform({
      x: platformImage.width * 3 + 436,
      y: 270,
      image: platformSmallTallImage,
    }),
    new Platform({
      x: platformImage.width * 5 + 636,
      y: 270,
      image: platformSmallTallImage,
    }),
    new Platform({
      x: platformImage.width * 5 + 436,
      y: 270,
      image: platformSmallTallImage,
    }),
    new Platform({ x: 1, y: 470, image: platformImage }),
    new Platform({ x: platformImage.width - 3, y: 470, image: platformImage }),
    new Platform({
      x: platformImage.width * 2 + 150,
      y: 470,
      image: platformImage,
    }),
    new Platform({
      x: platformImage.width * 3 + 149,
      y: 470,
      image: platformImage,
    }),
    new Platform({
      x: platformImage.width * 4 + 500,
      y: 470,
      image: platformImage,
    }),
    new Platform({
      x: platformImage.width * 5 + 500,
      y: 470,
      image: platformImage,
    }),
    new Platform({
      x: platformImage.width * 6 + 900,
      y: 470,
      image: platformImage,
    }),
  ];
  genericObjets = [
    new GenericObjet({ x: 0, y: 0, image: createImage(background) }),
    new GenericObjet({ x: 0, y: 50, image: createImage(hills) }),
    new GenericObjet({ x: 500, y: 100, image: createImage(hills) }),
  ];
  scrollOffset = 0;
}

const keys = {
  rigth: {
    pressed: false,
  },
  left: {
    pressed: false,
  },
};
init();
function animate() {
  requestAnimationFrame(animate);
  ctx.fillStyle = "white";
  ctx.clearRect(0, 0, canvas.width, canvas.height);

  genericObjets.forEach((genericObjet) => {
    genericObjet.draw();
  });

  plataforms.forEach((plataform) => {
    plataform.draw();
  });
  player.update();
  if (keys.rigth.pressed && player.position.x < 400) {
    player.velocity.x = player.speed;
  } else if (
    (keys.left.pressed && player.position.x > 100) ||
    (keys.left.pressed && scrollOffset === 0 && player.position.x > 0)
  ) {
    player.velocity.x = -player.speed;
  } else {
    player.velocity.x = 0;
    if (keys.rigth.pressed) {
      scrollOffset += player.speed;

      genericObjets.forEach((genericObjet) => {
        genericObjet.position.x -= 2;
      });

      plataforms.forEach((plataform) => {
        plataform.position.x -= player.speed;
      });
    } else if (keys.left.pressed && scrollOffset > 0) {
      scrollOffset -= player.speed;
      genericObjets.forEach((genericObjet) => {
        genericObjet.position.x += 2;
      });
      plataforms.forEach((plataform) => {
        plataform.position.x += player.speed;
      });
    }
  }

  plataforms.forEach((plataform) => {
    if (
      player.position.y + player.height <= plataform.position.y &&
      player.position.y + player.height + player.velocity.y >=
        plataform.position.y &&
      player.position.x + player.width >= plataform.position.x &&
      player.position.x + player.width <= plataform.position.x + plataform.width
    ) {
      player.velocity.y = 0;
    }
  });
  //win condition
  if (scrollOffset === 2000) {
    console.log("You Win");
  }
  //lose condition
  if (player.position.y > canvas.height) {
    init();
  }
}
animate();

window.addEventListener("keydown", ({ key }) => {
  switch (key) {
    case "w":
      player.velocity.y -= 35;
      break;
    case "a":
      keys.left.pressed = true;
      player.currentSprite = player.sprites.run.left;
      player.currentCropWidth = player.sprites.run.cropWidth;
      player.width = player.sprites.run.width
      break;
    case "s":
      player.velocity.y = 0;

      break;
    case "d":
      keys.rigth.pressed = true;
      player.currentSprite = player.sprites.run.rigth;
      player.currentCropWidth = player.sprites.run.cropWidth;
      player.width = player.sprites.run.width
      break;
    default:
      break;
  }
});

window.addEventListener("keyup", ({ key }) => {
  switch (key) {
    case "w":
      player.velocity.y = 0;
      break;
    case "a":
      keys.left.pressed = false;
      player.currentSprite = player.sprites.stand.left;
      player.currentCropWidth = player.sprites.stand.cropWidth;
      player.width = player.sprites.stand.width
      break;
    case "s":
      player.velocity.y = 0;
      break;
    case "d":
      keys.rigth.pressed = false;
      player.currentSprite = player.sprites.stand.rigth;
      player.currentCropWidth = player.sprites.stand.cropWidth;
      player.width = player.sprites.stand.width
      break;
    default:
      break;
  }
});
